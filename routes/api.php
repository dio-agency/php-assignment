<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users/{user}', 'UserController@show');
Route::post('/users', 'UserController@create');

// TODO: Endpoint for creating post
// TODO: Endpoint for creating comment
// TODO: Endpoint for retrieving all posts, sorted by updated_at, including comments, with creator of comment and user who created the post

// TODO: Include validation for creation endpoints
// TODO: Allow users to become friends
